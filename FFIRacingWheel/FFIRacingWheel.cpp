// FFIRacingWheel.cpp : Defines the exported functions for the DLL application.
//

#define _CRT_SECURE_NO_WARNINGS
#define DIRECTINPUT_VERSION 0x0800

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

#include <stdio.h>
#include <dinput.h>
#include <dinputd.h>

//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Joystick.rc
//
#define IDI_MAIN                        102
#define IDD_JOYST_IMM                   103
#define IDR_ACCELERATOR1                103
#define IDC_CLOSE                       1001
#define IDC_X_AXIS                      1010
#define IDC_Y_AXIS                      1011
#define IDC_Z_AXIS                      1012
#define IDC_X_AXIS_TEXT                 1013
#define IDC_Y_AXIS_TEXT                 1014
#define IDC_Z_AXIS_TEXT                 1015
#define IDC_X_ROT_TEXT                  1016
#define IDC_Y_ROT_TEXT                  1017
#define IDC_Z_ROT_TEXT                  1018
#define IDC_SLIDER0_TEXT                1019
#define IDC_X_ROT                       1020
#define IDC_Y_ROT                       1021
#define IDC_Z_ROT                       1022
#define IDC_SLIDER1_TEXT                1023
#define IDC_POV0_TEXT                   1024
#define IDC_POV1_TEXT                   1025
#define IDC_POV2_TEXT                   1026
#define IDC_POV3_TEXT                   1027
#define IDC_SLIDER0                     1030
#define IDC_SLIDER1                     1031
#define IDC_POV                         1040
#define IDC_POV0                        1040
#define IDC_BUTTONS                     1041
#define IDC_POV1                        1042
#define IDC_POV2                        1043
#define IDC_POV3                        1044

//-----------------------------------------------------------------------------
// Function-prototypes
//-----------------------------------------------------------------------------
HRESULT InitDirectInput(HWND hDlg);
BOOL CALLBACK    EnumObjectsCallback(const DIDEVICEOBJECTINSTANCE* pdidoi, VOID* pContext);
BOOL CALLBACK    EnumJoysticksCallback(const DIDEVICEINSTANCE* pdidInstance, VOID* pContext);
HRESULT UpdateInputState(void);

struct DI_ENUM_CONTEXT
{
	DIJOYCONFIG* pPreferredJoyCfg;
	bool bPreferredJoyCfgValid;
};

struct JoystickCapabilities {
	int X_AXIS = 0;
	int Y_AXIS = 0;
	int Z_AXIS = 0;
	int X_ROT = 0;
	int Y_ROT = 0;
	int Z_ROT = 0;
	int SLIDER0 = 0;
	int SLIDER1 = 0;
	int POV0 = 0;
	int POV1 = 0;
	int POV2 = 0;
	int POV3 = 0;
};

JoystickCapabilities joystickCapabilities;

struct WheelState {
	long wheel;
	long gasPedal;
	long brakePedal;
	int gearPlusPressed;
	int gearMinusPressed;
	int hornPressed;
};

extern "C" __declspec(dllexport) int InitializeJoystick(void);
extern "C" __declspec(dllexport) JoystickCapabilities GetJoystickCapabilities(void);
extern "C" __declspec(dllexport) int UpdateJoystickState(void);
extern "C" __declspec(dllexport) WheelState GetWheelState(void);
extern "C" __declspec(dllexport) void ReleaseJoystick(void);
extern "C" __declspec(dllexport) char* GetActiveWindowName();

//-----------------------------------------------------------------------------
// Defines, constants, and global variables
//-----------------------------------------------------------------------------
#define SAFE_DELETE(p)  { if(p) { delete (p);     (p)=nullptr; } }
#define SAFE_RELEASE(p) { if(p) { (p)->Release(); (p)=nullptr; } }

LPDIRECTINPUT8          g_pDI = nullptr;
LPDIRECTINPUTDEVICE8    g_pJoystick = nullptr;

DIJOYSTATE2 joystickState;           // DInput joystick state 

static char name[2048];

char* GetActiveWindowName() {
	HWND h = GetActiveWindow();

	GetWindowTextA(h, name, sizeof(name));

	return name;
}

int InitializeJoystick(void) {

	InitDirectInput(GetActiveWindow());

	return g_pJoystick != nullptr;
}

JoystickCapabilities GetJoystickCapabilities(void) {
	return joystickCapabilities;
}

int UpdateJoystickState(void) {
	return UpdateInputState() == S_OK;
}

WheelState GetWheelState(void) {

	WheelState state;

	state.wheel = joystickState.lX;
	state.gasPedal = joystickState.lY;
	state.brakePedal = joystickState.lRz;
	state.gearPlusPressed = ((joystickState.rgbButtons[12] & 0x80) != 0);
	state.gearMinusPressed = ((joystickState.rgbButtons[13] & 0x80) != 0);
	state.hornPressed = ((joystickState.rgbButtons[19] & 0x80) != 0);
	return state;
}

HRESULT InitDirectInput(HWND hDlg)
{
	HRESULT hr;

	// Register with the DirectInput subsystem and get a pointer
	// to a IDirectInput interface we can use.
	// Create a DInput object
	if (FAILED(hr = DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION,
		IID_IDirectInput8, (VOID**)&g_pDI, nullptr)))
		return hr;

	DIJOYCONFIG PreferredJoyCfg = { 0 };
	DI_ENUM_CONTEXT enumContext;
	enumContext.pPreferredJoyCfg = &PreferredJoyCfg;
	enumContext.bPreferredJoyCfgValid = false;

	IDirectInputJoyConfig8* pJoyConfig = nullptr;
	if (FAILED(hr = g_pDI->QueryInterface(IID_IDirectInputJoyConfig8, (void**)&pJoyConfig)))
		return hr;

	PreferredJoyCfg.dwSize = sizeof(PreferredJoyCfg);
	if (SUCCEEDED(pJoyConfig->GetConfig(0, &PreferredJoyCfg, DIJC_GUIDINSTANCE))) // This function is expected to fail if no joystick is attached
		enumContext.bPreferredJoyCfgValid = true;
	SAFE_RELEASE(pJoyConfig);

	// Look for a simple joystick we can use for this sample program.
	if (FAILED(hr = g_pDI->EnumDevices(DI8DEVCLASS_GAMECTRL,
		EnumJoysticksCallback,
		&enumContext, DIEDFL_ATTACHEDONLY)))
		return hr;

	// Make sure we got a joystick
	if (!g_pJoystick)
	{
		return S_FALSE;
	}

	// Set the data format to "simple joystick" - a predefined data format 
//
// A data format specifies which controls on a device we are interested in,
// and how they should be reported. This tells DInput that we will be
// passing a DIJOYSTATE2 structure to IDirectInputDevice::GetDeviceState().
	if (FAILED(hr = g_pJoystick->SetDataFormat(&c_dfDIJoystick2)))
		return hr;

	// Set the cooperative level to let DInput know how this device should
	// interact with the system and with other DInput applications.
	if (FAILED(hr = g_pJoystick->SetCooperativeLevel(hDlg, DISCL_EXCLUSIVE |
		DISCL_FOREGROUND)))
		return hr;

	// Enumerate the joystick objects. The callback function enabled user
	// interface elements for objects that are found, and sets the min/max
	// values property for discovered axes.
	if (FAILED(hr = g_pJoystick->EnumObjects(EnumObjectsCallback,
		(VOID*)hDlg, DIDFT_ALL)))
		return hr;

	
	return S_OK;
}

	//-----------------------------------------------------------------------------
	// Name: EnumJoysticksCallback()
	// Desc: Called once for each enumerated joystick. If we find one, create a
	//       device interface on it so we can play with it.
	//-----------------------------------------------------------------------------
	BOOL CALLBACK EnumJoysticksCallback(const DIDEVICEINSTANCE* pdidInstance,
		VOID* pContext)
	{
		auto pEnumContext = reinterpret_cast<DI_ENUM_CONTEXT*>(pContext);
		HRESULT hr;

		// Skip anything other than the perferred joystick device as defined by the control panel.  
		// Instead you could store all the enumerated joysticks and let the user pick.
		if (pEnumContext->bPreferredJoyCfgValid &&
			!IsEqualGUID(pdidInstance->guidInstance, pEnumContext->pPreferredJoyCfg->guidInstance))
			return DIENUM_CONTINUE;

		// Obtain an interface to the enumerated joystick.
		hr = g_pDI->CreateDevice(pdidInstance->guidInstance, &g_pJoystick, nullptr);

		// If it failed, then we can't use this joystick. (Maybe the user unplugged
		// it while we were in the middle of enumerating it.)
		if (FAILED(hr))
			return DIENUM_CONTINUE;

		// Stop enumeration. Note: we're just taking the first joystick we get. You
		// could store all the enumerated joysticks and let the user pick.
		return DIENUM_STOP;
	}

	//-----------------------------------------------------------------------------
	// Name: EnumObjectsCallback()
	// Desc: Callback function for enumerating objects (axes, buttons, POVs) on a 
	//       joystick. This function enables user interface elements for objects
	//       that are found to exist, and scales axes min/max values.
	//-----------------------------------------------------------------------------
	BOOL CALLBACK EnumObjectsCallback(const DIDEVICEOBJECTINSTANCE* pdidoi,
		VOID* pContext)
	{
		HWND hDlg = (HWND)pContext;

		static int nSliderCount = 0;  // Number of returned slider controls
		static int nPOVCount = 0;     // Number of returned POV controls

		// For axes that are returned, set the DIPROP_RANGE property for the
		// enumerated axis in order to scale min/max values.
		if (pdidoi->dwType & DIDFT_AXIS)
		{
			DIPROPRANGE diprg;
			diprg.diph.dwSize = sizeof(DIPROPRANGE);
			diprg.diph.dwHeaderSize = sizeof(DIPROPHEADER);
			diprg.diph.dwHow = DIPH_BYID;
			diprg.diph.dwObj = pdidoi->dwType; // Specify the enumerated axis
			diprg.lMin = -1000;
			diprg.lMax = +1000;

			// Set the range for the axis
			if (FAILED(g_pJoystick->SetProperty(DIPROP_RANGE, &diprg.diph)))
				return DIENUM_STOP;

		}

		// Set the UI to reflect what objects the joystick supports
		if (pdidoi->guidType == GUID_XAxis)
		{
			joystickCapabilities.X_AXIS = 1;
		}
		if (pdidoi->guidType == GUID_YAxis)
		{
			joystickCapabilities.Y_AXIS = 1;
		}
		if (pdidoi->guidType == GUID_ZAxis)
		{
			joystickCapabilities.Z_AXIS = 1;
		}
		if (pdidoi->guidType == GUID_RxAxis)
		{
			joystickCapabilities.X_ROT = 1;
		}
		if (pdidoi->guidType == GUID_RyAxis)
		{
			joystickCapabilities.Y_ROT = 1;
		}
		if (pdidoi->guidType == GUID_RzAxis)
		{
			joystickCapabilities.Z_ROT = 1;
		}
		if (pdidoi->guidType == GUID_Slider)
		{
			switch (nSliderCount++)
			{
			case 0:
				joystickCapabilities.SLIDER0 = 1;
				break;

			case 1:
				joystickCapabilities.SLIDER1 = 1;
				break;
			}
		}
		if (pdidoi->guidType == GUID_POV)
		{
			switch (nPOVCount++)
			{
			case 0:
				joystickCapabilities.POV0 = 1;
				break;

			case 1:
				joystickCapabilities.POV1 = 1;
				break;

			case 2:
				joystickCapabilities.POV2 = 1;
				break;

			case 3:
				joystickCapabilities.POV3 = 1;
				break;
			}
		}

		return DIENUM_CONTINUE;
	}

	//-----------------------------------------------------------------------------
	// Name: UpdateInputState()
	// Desc: Get the input device's state and display it.
	//-----------------------------------------------------------------------------
	HRESULT UpdateInputState(void)
	{
		HRESULT hr;
	
		
		if (!g_pJoystick)
			return S_OK;

		// Poll the device to read the current state
		hr = g_pJoystick->Poll();
		if (FAILED(hr))
		{
			// DInput is telling us that the input stream has been
			// interrupted. We aren't tracking any state between polls, so
			// we don't have any special reset that needs to be done. We
			// just re-acquire and try again.
			hr = g_pJoystick->Acquire();
			while (hr == DIERR_INPUTLOST)
				hr = g_pJoystick->Acquire();

			// hr may be DIERR_OTHERAPPHASPRIO or other errors.  This
			// may occur when the app is minimized or in the process of 
			// switching, so just try again later 
			//return S_OK;
			return hr;
		}

		// Get the input's device state
		if (FAILED(hr = g_pJoystick->GetDeviceState(sizeof(DIJOYSTATE2), &joystickState)))
			return hr; // The device should have been acquired during the Poll()


		return S_OK;
	}

	void ReleaseJoystick(void)
	{
		
		// Unacquire the device one last time just in case 
		// the app tried to exit while the device is still acquired.
		if (g_pJoystick)
			g_pJoystick->Unacquire();

		// Release any DirectInput objects.
		SAFE_RELEASE(g_pJoystick);
		SAFE_RELEASE(g_pDI);
	}

	
